
function Nav() {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">Conference GO!</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <a className="nav-link active" aria-current="page" href="#">Home</a>
            <a className="nav-link color" aria-current="page" href="new-location.html">New Location</a>
            <a className="nav-link" aria-current="page" href="new-conference.html">New Conference</a>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Nav;

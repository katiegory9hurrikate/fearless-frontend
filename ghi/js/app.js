function createCard(name, description, pictureUrl, starts, ends) {
  return `
  <div class="col-4">
    <div class="card mb-4 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer bg-transparent border-success">${starts} - ${ends}</div>
    </div>
  </div>
  `;
}

function showAlert(message, type) {
  const alertContainer = document.querySelector('.alert-container');
  const alertHTML = `
    <div class="alert alert-${type} alert-dismissible fade show" role="alert">
      ${message}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  `;
  alertContainer.innerHTML += alertHTML;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      showAlert("Failed to retrieve conferences: " + response.status + " " + response.statusText, "danger");
    } else {
      const data = await response.json();

    for (let conference of data.conferences) {
      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);
      if (detailResponse.ok) {
        const details = await detailResponse.json();
        const title = details.conference.name;
        const desc = details.conference.description;
        const imgSource = details.conference.location.picture_url;
        const startDate = new Date(details.conference.starts);
        const startMonth = startDate.getMonth();
        const startDay = startDate.getDay() + 1;
        const startYear = startDate.getFullYear();
        const shortDate = `${startMonth}/${startDay}/${startYear}`;
        const endDate = new Date(details.conference.ends);
        const endMonth = endDate.getMonth();
        const endDay = endDate.getDay() + 1;
        const endYear = endDate.getFullYear();
        const shortDateEnd = `${endMonth}/${endDay}/${endYear}`;
        const html = createCard(title, desc, imgSource, shortDate, shortDateEnd);
        const row = document.querySelector('.row');
        row.innerHTML += html;
      } else {
        showAlert("Failed to retrieve conference details: " + detailResponse.status + " " + detailResponse.statusText, "warning");
      }
    }
  }
  } catch (e) {
    showAlert("Oops! Something went wrong! " + e.message, "danger");
  }

});

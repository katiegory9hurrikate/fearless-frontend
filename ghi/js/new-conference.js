window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (!response.ok){
        console.error('Failed to fetch locations: ', response.status, response.statusText);
    } else {
        const data = await response.json();
        const conferenceTag = document.getElementById('location');

        for (let location of data.locations){
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            conferenceTag.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const dataObject = Object.fromEntries(formData);
        const json = JSON.stringify(dataObject);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});
